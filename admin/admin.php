<?php
/**
 * Created by PhpStorm.
 * User: valleyview
 * Date: 28/03/2014
 * Time: 10:32
 */

add_action( 'login_enqueue_scripts', function() {
    if (current_theme_supports('lolly-theme')) {
        bd_lolly_theme();
    } elseif(current_theme_supports('bd-theme')) {
        $theme = get_theme_support('bd-theme');
        switch ($theme[0][0]) {
            case "merchant":
                bd_merchant_theme();
                break;
            case "lolly":
                bd_lolly_theme();
                break;
            case "core":
                bd_core_theme();
                break;
            case "none":
                break;
            default:
                bd_bd_theme();
                break;
        }
    } else {
        bd_bd_theme();
    }
}, 100);

function bd_common_css() {
    ?>
    <style type="text/css">
        body.login {
            display: -ms-flexbox;
            display: -webkit-flex;
            display: flex;
            -ms-flex-align: center;
            -webkit-align-items: center;
            -webkit-box-align: center;
            align-items: center;
        }
        body.login #login {
            padding: 3% 0 5% 0;
        }
        body.login h1 a {
            background-size: auto;
            width: auto;
            height: 125px;
            margin-bottom: 40px;
        }
        .login p#backtoblog {
            display: none;
        }
        .login.login-action-lostpassword p#nav {
            display: none;
        }
        .login.login-action-login p#nav {
            text-align: center;
        }
        body.login #nav a, body.login #backtoblog a {
            font-size: 15px;
        }
        body.wp-core-ui .button-primary[disabled], body.wp-core-ui .button-primary:disabled, body.wp-core-ui .button-primary.button-primary-disabled, body.wp-core-ui .button-primary.disabled {
            text-shadow: none !important;
        }
    </style>
    <?php
}

function bd_bd_theme(){
    bd_common_css();
    ?>

    <style type="text/css">
        body.login {
            background: #8444fd;
            background: radial-gradient(circle, rgba(108,57,204,1) 0%, rgba(132,68,253,1) 50%);
        }

        body.login h1 {
            margin: 0 50px;
        }

        body.login form {
            padding: 24px;
            box-shadow: none;
        }

        body.login h1 a {
            background-image: url(<?php echo plugins_url( 'admin/sequel.svg' , dirname(__FILE__) ) ?>);
            height: 103px;
            background-size: contain;
        }

        body.login #nav {
            margin-top: 40px;
        }

        body.login #nav a, body.login #backtoblog a {
            color: #fff;
        }
        body.login #nav a:hover, body.login #backtoblog a:hover {
            color: #8F69C9;
        }

        body.login .privacy-policy-page-link {
            margin-top: 10px;
        }

        body.login .privacy-policy-page-link a {
            color: #7f7f7f;
            font-size: 15px;
            text-decoration: none;
            font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen-Sans, Ubuntu, Cantarell, "Helvetica Neue", sans-serif;
        }

        body.login .privacy-policy-page-link a:hover {
            color: #9f9f9f
        }

        body.login .button-primary {
            border-radius: 0;
            font-weight: 500;
            text-transform: uppercase;
            background: #8444fd;
            border-color: #6b2be4 #6b2be4 #6b2be4;
            box-shadow: 0 1px 0 #6b2be4;
            text-shadow: 0 -1px 1px #6b2be4, 1px 0 1px #6b2be4, 0 1px 1px #6b2be4, -1px 0 1px #6b2be4;
        }
        body.login .button-primary:hover, body.login .button-primary:focus {
            background: #9E5EFF;
            border-color: #6b2be4;
            box-shadow: 0 1px 0 #6b2be4;
        }

        body input[type="checkbox"], body input[type="text"], body input[type="password"] {
            box-shadow: none;
        }

        body input[type="checkbox"]:checked:before {
            color: #8444fd;
        }
        body input[type="checkbox"]:focus, body input[type="text"]:focus, body input[type="password"]:focus {
            border-color: #9E5EFF;
            box-shadow: none;
        }

    </style>


    <?php
}


function bd_lolly_theme() {
    ?>

    <style type="text/css">
        body.login {
            background-color: #f4f4f4;
        }
        body.login h1 a {
            background-image: url(<?php echo plugins_url( 'admin/lolly.svg' , dirname(__FILE__) ) ?>);
        }

        body.login #nav a, body.login #backtoblog a {
            color: #72777c;
        }
        body.login #nav a:hover, body.login #backtoblog a:hover {
            color: #999999;
        }

        body.wp-core-ui .button-primary {
            background: #89a5d5;
            border-color: #6388c8 #517ac1 #517ac1;
            box-shadow: 0 1px 0 #517ac1;
            text-shadow: 0 -1px 1px #517ac1, 1px 0 1px #517ac1, 0 1px 1px #517ac1, -1px 0 1px #517ac1;
        }
        body.wp-core-ui .button-primary:hover, body.wp-core-ui .button-primary:focus {
            background: #94aed9;
            border-color: #517ac1;
            box-shadow: 0 1px 0 #517ac1;
        }
        body.wp-core-ui .button-primary:focus {
            box-shadow: inset 0 1px 0 #6388c8, 0 0 2px 1px #33b3db;
        }
        body.wp-core-ui .button-primary:active {
            background: #6388c8;
            border-color: #517ac1;
            box-shadow: inset 0 2px 0 #517ac1;
        }
        body.wp-core-ui .button-primary[disabled], body.wp-core-ui .button-primary:disabled, body.wp-core-ui .button-primary.button-primary-disabled, body.wp-core-ui .button-primary.disabled {
            color: #c7cbd1 !important;
            background: #6b8eca !important;
            border-color: #517ac1 !important;
        }
        body.wp-core-ui .button-primary.button-hero {
            box-shadow: 0 2px 0 #517ac1 !important;
        }
        body.wp-core-ui .button-primary.button-hero:active {
            box-shadow: inset 0 3px 0 #517ac1 !important;
        }

        body input[type="checkbox"]:checked:before {
            color: #89a5d5;
        }
        body input[type="checkbox"]:focus, body input[type="text"]:focus {
            border-color: #89a5d5;
            -webkit-box-shadow: 0 0 2px rgba( 137, 165, 213, 0.8 );
            box-shadow: 0 0 2px rgba( 137, 165, 213, 0.8 );
        }

    </style>

    <?php
}

function bd_core_theme() {
    bd_common_css();

    ?>
    <style type="text/css">
        body.login {
            background-color: #ff475f;
        }
        body.login h1 a {
            background-image: url(<?php echo plugins_url( 'admin/core.png' , dirname(__FILE__) ) ?>);
            background-size: auto;
            width: auto;
        }
        body.login #nav a, body.login #backtoblog a {
            color: #4f4d4e;
            font-size: 15px;
        }
        body.login #nav a:hover, body.login #backtoblog a:hover {
            color: #222222;
        }
        body.wp-core-ui .button-primary {
            background: #ff475f;
            border-color: #ff1433 #fa0021 #fa0021;
            color: white;
            box-shadow: 0 1px 0 #fa0021;
            text-shadow: 0 -1px 1px #fa0021, 1px 0 1px #fa0021, 0 1px 1px #fa0021, -1px 0 1px #fa0021;
        }
        body.wp-core-ui .button-primary:hover, body.wp-core-ui .button-primary:focus {
            background: #ff566c;
            border-color: #fa0021;
            color: white;
            box-shadow: 0 1px 0 #fa0021;
        }
        body.wp-core-ui .button-primary:focus {
            box-shadow: inset 0 1px 0 #ff1433, 0 0 2px 1px #33b3db;
        }
        body.wp-core-ui .button-primary:active {
            background: #ff1433;
            border-color: #fa0021;
            box-shadow: inset 0 2px 0 #fa0021;
        }
        body.wp-core-ui .button-primary[disabled], body.wp-core-ui .button-primary:disabled, body.wp-core-ui .button-primary.button-primary-disabled, body.wp-core-ui .button-primary.disabled {
            color: #d1c7c8 !important;
            background: #ff1e3c !important;
            border-color: #fa0021 !important;
        }
        body.wp-core-ui .button-primary.button-hero {
            box-shadow: 0 2px 0 #fa0021 !important;
        }
        body.wp-core-ui .button-primary.button-hero:active {
            box-shadow: inset 0 3px 0 #fa0021 !important;
        }
        body input[type="checkbox"]:checked:before {
            color: #ff475f;
        }
        body input[type="checkbox"]:focus, body input[type="text"]:focus {
            border-color: #ff475f;
            -webkit-box-shadow: 0 0 2px rgba( 252, 74, 98, 0.8 );
            box-shadow: 0 0 2px rgba( 252, 74, 98, 0.8 );
        }
    </style>
    <?php
}
