(function ($) {

    var CC_Modals = {

        element: '[data-modal]',

        init: function() {

            $(document).on('click', CC_Modals.element, CC_Modals.setup);

        },

        spinner: function() {

            return new Spinner({
                lines: 11, // The number of lines to draw
                length: 13, // The length of each line
                width: 8, // The line thickness
                radius: 18, // The radius of the inner circle
                corners: 1, // Corner roundness (0..1)
                rotate: 0, // The rotation offset
                direction: 1, // 1: clockwise, -1: counterclockwise
                color: '#000', // #rgb or #rrggbb or array of colors
                speed: 1, // Rounds per second
                trail: 60, // Afterglow percentage
                shadow: false, // Whether to render a shadow
                hwaccel: true, // Whether to use hardware acceleration
                className: 'spinner', // The CSS class to assign to the spinner
                zIndex: 2e9, // The z-index (defaults to 2000000000)
                top: '50%', // Top position relative to parent
                left: '50%' // Left position relative to parent
            }).spin();

        },

        overlay: function() {

            var $overlay = $("<div class='temp-overlay' />").html("<div class='wrapper'></div>");
            $("body").append($overlay);
            return $('.temp-overlay');

        },

        clearLoader: function($overlay, $spinner, instant) {

            if(typeof instant == undefined)
                var instant = false;

            if(instant) {

                setTimeout(function() {
                    $spinner.stop();
                    $overlay.remove();
                }, 110);

            } else {
                $overlay.fadeOut(100, function () {
                    $spinner.stop();
                    $overlay.remove();
                });
            }
            return true;

        },

        clear: function() {



        },

        setup: function(e) {
            e.preventDefault();

            var _modal_name = $(this).data('modal');
            var _params = "";
            if($(this).data('modal-params')) {
                _params = $(this).data('modal-params');
            }
            CC_Modals.run(_modal_name, _params);

        },

        reposition: function() {
            var modal = $(this),
                dialog = modal.find('.modal-dialog');
            modal.css('display', 'block');

            // Dividing by two centers the modal exactly, but dividing by three
            // or four works better for larger screens.
            dialog.css("margin-top", Math.max(0, ($(window).height() - dialog.height()) / 2));
        },

        run: function(_modal_name, _params) {
            var _load_has_been_stopped = false;

            var _ajax_url = cc_data.ajax_url;

            var $spinner = CC_Modals.spinner();

            var $overlay = CC_Modals.overlay();
            $overlay.find('div.wrapper').append($spinner.el);

            $overlay.on('click', function() {
                _load_has_been_stopped = CC_Modals.clearLoader($overlay, $spinner);
            });

            $.ajax(_ajax_url, {
                type: 'get',
                dataType: "html",
                data: {
                    action: "cc_modal_load",
                    modal: _modal_name,
                    params: _params
                },
                success: function(data) {

                    if(!_load_has_been_stopped) {

                        var $modal = $(data);
                        var _id = $modal.attr('id');

                        _load_has_been_stopped = CC_Modals.clearLoader($overlay, $spinner, true);

                        var $_modal_instance = $modal.modal({
                            show: false
                        });

                        $_modal_instance.on('hidden.bs.modal', function() {
                            $modal.remove();
                            $("#" + _id).remove();
                        });

                        $_modal_instance.on('shown.bs.modal', CC_Modals.reposition);

                        // Reposition when the window is resized
                        $(window).on('resize', function() {
                            $('.modal:visible').each(CC_Modals.reposition);
                        });

                        $(document).trigger('modal-load', [{
                            modal: $_modal_instance
                        }]);

                        $_modal_instance.modal('show');

                    }

                },
                error: function(jqXHR, textStatus, errorThrown) {

                    alert('There was an error. Please try again later. (' + errorThrown + ')');
                    _load_has_been_stopped = CC_Modals.clearLoader($overlay, $spinner);

                }
            });

        }

    };
    window.CC_Modals = CC_Modals;

})(jQuery); // Fully reference jQuery after this point.


jQuery(document).ready(function($) {

    window.CC_Modals.init();

});
