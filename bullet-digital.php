<?php
/*
Plugin Name: Bullet Digital
Version: 1
Plugin URI: http://bulletdigital.co.uk/
Author: Oliver Sadler - Bullet Digital
Author URI: http://bulletdigital.co.uk/
Description: Set of useful utilities
*/
include 'admin/admin.php';
include 'cc.class.php';
include 'utils.php';

include 'cc_form/cc_form.php';
include 'cc_modal/cc_modal.php';
